---
include:
  - project: "empaia/integration/ci-templates"
    ref: "main"
    file:
      - "pipeline-templates/pipeline-base.yml"
      - "job-templates/bot-add-release-version-tag.yml"
      - "job-templates/check-existing-docker.yml"
      - "job-templates/docker-image-scan.yml"
      - "job-templates/poetry-codecheck.yml"
      - "job-templates/poetry-get-version.yml"
      - "job-templates/poetry-pytest.yml"
      - "job-templates/poetry-pytest-compose.yml"


variables:
  TITLE: "KGSA"
  DESCRIPTION: "Kidney Glomeruli Segmentation App (KGSA)"
  DIRECTORY: "glomeruli_segmentation"
  TEST_DIRECTORY: "tests"
  RUNNER_TAG: "empaia-parallel"
  RUNNER_DOCKER_TAG: "empaia-docker"

test:
  tags:
    - $RUNNER_TAG
  image: registry.gitlab.com/empaia/integration/ci-docker-images/test-runner
  before_script:
    - poetry install
    # installing torch separately because of only cpu support of the test runner
    - poetry run pip3 uninstall torch torchvision -y
    - poetry run pip3 install torch==2.0.1+cpu --index-url https://download.pytorch.org/whl/cpu 
    # install cv2 dependencies
    - apt update && apt install -y --no-install-recommends libgl1
  script:
    - poetry run pytest -v tests

docker-build-dev:
  image:
    name: gcr.io/kaniko-project/executor:v1.9.2-debug
    entrypoint: [""]
  tags:
    - $RUNNER_TAG
  script:
    - mkdir -p /kaniko/.docker
    - >-
      echo
      "{\"auths\":
      {\"${CI_REGISTRY}\":
      {\"auth\":
      \"$(printf "%s:%s"
      "${CI_REGISTRY_USER}"
      "${CI_REGISTRY_PASSWORD}"
      | base64 | tr -d '\n')\"}},
      \"index.docker.io\":
      {\"auth\":
      \"$(printf "%s:%s"
      "${DOCKERHUB_REGISTRY_USER}"
      "${DOCKERHUB_REGISTRY_TOKEN}"
      | base64 | tr -d '\n')\"}}
      }" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --skip-unused-stages=true
      --cache=true 
      --compressed-caching=false 
      --use-new-run 
      --cleanup
      --build-arg "http_proxy=${http_proxy}"
      --build-arg "HTTP_PROXY=${HTTP_PROXY}"
      --build-arg "https_proxy=${https_proxy}"
      --build-arg "HTTPS_PROXY=${HTTPS_PROXY}"
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR/Dockerfile"
      --destination "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME-dev"


poetry-get-version:
  extends: .poetry-get-version

poetry-codecheck:
  extends: .poetry-codecheck

poetry-pytest:
  extends: .poetry-pytest

poetry-pytest-compose:
  extends: .poetry-pytest-compose

docker-image-scan:
  extends: .docker-image-scan
  needs:
    - job: docker-build-dev

check-existing-docker:
  extends: .check-existing-docker

docker-build-and-push:
  tags:
    - $RUNNER_TAG
  rules:
    - if: $CI_COMMIT_REF_NAME == "main"
    - if: $CI_COMMIT_REF_NAME == "master"
  image:
    name: gcr.io/kaniko-project/executor:v1.9.2-debug
    entrypoint: [""]
  script:
    - mkdir -p /kaniko/.docker
    - >-
      echo
      "{\"auths\":
      {\"${CI_REGISTRY}\":
      {\"auth\":
      \"$(printf "%s:%s"
      "${CI_REGISTRY_USER}"
      "${CI_REGISTRY_PASSWORD}"
      | base64 | tr -d '\n')\"}},
      \"index.docker.io\":
      {\"auth\":
      \"$(printf "%s:%s"
      "${DOCKERHUB_REGISTRY_USER}"
      "${DOCKERHUB_REGISTRY_TOKEN}"
      | base64 | tr -d '\n')\"}}
      }" > /kaniko/.docker/config.json
    - >-
      /kaniko/executor
      --skip-unused-stages=true
      --cache=true 
      --compressed-caching=false 
      --use-new-run 
      --cleanup
      --build-arg "http_proxy=${http_proxy}"
      --build-arg "HTTP_PROXY=${HTTP_PROXY}"
      --build-arg "https_proxy=${https_proxy}"
      --build-arg "HTTPS_PROXY=${HTTPS_PROXY}"
      --context "$CI_PROJECT_DIR"
      --dockerfile "$CI_PROJECT_DIR/Dockerfile"
      --destination "$CI_REGISTRY_IMAGE"
      --destination "$CI_REGISTRY_IMAGE:$VERSION"
      --label org.opencontainers.image.authors="EMPAIA"
      --label org.opencontainers.image.vendor="EMPAIA"
      --label org.opencontainers.image.version="$VERSION"
      --label org.opencontainers.image.source=$CI_PROJECT_URL
      --label org.opencontainers.image.revision=$CI_COMMIT_SHA
      --label org.opencontainers.image.title="$TITLE"
      --label org.opencontainers.image.description="$DESCRIPTION"
  needs:
    - job: poetry-get-version
      artifacts: true
    - job: poetry-codecheck
      optional: true
    - job: poetry-pytest
      optional: true
    - job: poetry-pytest-compose
      optional: true
    - job: docker-image-scan
    - job: check-existing-docker
      optional: true
    - job: test

.bot-add-release-version-tag-default-needs:
  needs:
    - job: docker-build-and-push
      optional: true
    - job: poetry-get-version
      artifacts: true

bot-add-release-version-tag:
  extends: .bot-add-release-version-tag
  needs:
    - !reference [.bot-add-release-version-tag-default-needs, needs]
