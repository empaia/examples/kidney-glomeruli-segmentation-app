# build stage
FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.0@sha256:81523e4a90baa876e4f6411c66566ff246974457c4034d5924bd6c63de76e2af AS builder

# Download model weights

WORKDIR /tmp
RUN mkdir -p model && curl -OJ https://nx9836.your-storageshare.de/s/gaqByayBJXcBmPQ/download && mv glomeruli_segmentation_16934_best_metric.model-384e1332.pth model/
RUN mkdir -p checkpoints &&  curl -OJ https://download.pytorch.org/models/resnet34-b627a593.pth \
    && mv resnet34-b627a593.pth /tmp/checkpoints/ 

# build module
COPY . /root/app
WORKDIR /root/app
RUN poetry export --without-hashes -f requirements.txt > requirements.txt
RUN poetry build

# install stage
FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.1@sha256:b7e4101980a21d2c5ac69a660ed3d3eb960d2993501ec256f1bd2e7b7c9d40d1
USER root

RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    libgl1-mesa-glx \
    libglib2.0-0 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && :

USER appuser
COPY --from=builder /tmp/model/* /model/
ENV MODEL_PATH /model/glomeruli_segmentation_16934_best_metric.model-384e1332.pth
COPY --from=builder /tmp/checkpoints/* /home/appuser/.cache/torch/hub/checkpoints/
COPY --from=builder /root/app/dist/*.whl /tmp
RUN pip3 install --no-cache-dir /tmp/*.whl

# Copy configuration file
COPY configuration.json /tmp
ENV CONFIG_PATH /tmp/configuration.json

CMD ["python", "-m", "glomeruli_segmentation", "-v"]
