from typing import Callable, List, Tuple, Union

import cv2 as cv
import numpy as np
import shapely.geometry as sg
import torch

from glomeruli_segmentation.data_classes import Mask

_Contour = List[List[int]]


def find_contours(mask: Mask) -> List[_Contour]:
    """
    Finds contours in a single-channel image. Treats any non-zero value as True, and zeros as False.

    :param binary_mask_image: A single-channel image.
    :return: A list of contours.
    """
    if mask.image.dtype in (np.half, np.single, np.double, torch.half, torch.float, torch.double):
        image = np.array(mask.image[:] * 255, dtype=np.uint8)
    image = image.squeeze()
    contours, _ = cv.findContours(
        image=image, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_SIMPLE, offset=mask.rect.upper_left
    )
    return [contour.squeeze() for contour in contours]


def classify_annotations(
    confidences: List[float], condition: Callable[..., bool], class_if_true: str, class_if_false: str
) -> Tuple[List[str], int, int]:
    """
    Classifies annotations based on a condition and an annotation confidence.
    class_if_true is assigned to annotations that satisfy the condition, and
    class_if_false is assigned to those that do not.

    :param confidences: A list of confidences.
    :param condition: A function that takes a confidence and returns a boolean.
    :param class_if_true: The class to assign to the annotations that satisfy the condition.
    :param class_if_false: The class to assign to the annotations that do not satisfy the condition.
    :return: A tuple containing:
        - A list of classifications.
        - The number of positive classifications.
        - The number of negative classifications.
    """

    classifications = []
    num_positive_classifications = 0
    num_negative_classifications = 0

    for confidence in confidences:
        if condition(confidence):
            classification = class_if_true
            num_positive_classifications += 1
        else:
            classification = class_if_false
            num_negative_classifications += 1
        classifications.append(classification)

    return classifications, num_positive_classifications, num_negative_classifications


def merge_overlapping_polygons(polygons: List[Union[_Contour, sg.Polygon]]) -> List[sg.Polygon]:
    """
    Merges overlapping polygons, converting them to shapely polygons if they are not already.

    :param polygons: A list of polygons.
    :return: A list of merged sg.Polygons.
    """

    polygons = [sg.Polygon(polygon).buffer(0) for polygon in polygons if len(polygon) > 2]
    if not polygons:
        return []

    merged_polygon = polygons.pop()
    for polygon in polygons:
        merged_polygon = merged_polygon.union(polygon)
    return merged_polygon


def average_values_in_polygon(segmentation: Mask, polygon: sg.Polygon) -> float:
    """
    Calculates the average value in a polygon.

    :param segmentation: A non-binary segmentation mask.
    :param polygon: A polygon.
    :return: The average value of pixels from the segmentation mask within the polygon.
    """

    offset = segmentation.rect.upper_left
    x_min, y_min, x_max, y_max = polygon.bounds

    x_min, x_max = int(x_min) - offset[0], int(x_max) - offset[0]
    y_min, y_max = int(y_min) - offset[1], int(y_max) - offset[1]

    values = segmentation.image[y_min:y_max, x_min:x_max]

    average_value = np.nanmean(values[values > 0])
    return average_value if not np.isnan(average_value) else 0.0
