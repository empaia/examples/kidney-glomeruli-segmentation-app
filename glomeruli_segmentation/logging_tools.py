import logging
import sys

DEFAULT_FORMATTER = logging.Formatter("%(asctime)s | %(name)-6s | %(levelname)-6s | %(message)s")

log_level_names = {
    logging.CRITICAL: "CRITICAL",
    logging.ERROR: "ERROR",
    logging.WARNING: "WARNING",
    logging.INFO: "INFO",
    logging.DEBUG: "DEBUG",
    logging.NOTSET: "NOTSET",
}


def get_log_level_for_verbosity(
    verbosity: int = 0, default_log_level: int = logging.WARN, min_log_level: int = logging.DEBUG
) -> int:
    """
    Returns the log level for the given verbosity.

    :param verbosity: The verbosity, usually set as a count of -v flags on the app entrypoint.
    :param default_log_level: The default log level.
    :param min_log_level: The minimum log level.

    :return: The log level.
    """
    return max(default_log_level - 10 * verbosity, min_log_level)


def get_console_handler(formatter: logging.Formatter = DEFAULT_FORMATTER) -> logging.Handler:
    """
    Returns a console handler with the given formatter.

    The handler is configured to send logs to stderr.

    :param formatter: The formatter to use.
    :return: A console handler.
    """
    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)
    return console_handler


def get_logger(
    logger_name: str = __name__,
    log_level: int = get_log_level_for_verbosity(0),
    propagate: bool = False,
    handler: logging.Handler = get_console_handler(),
) -> logging.Logger:
    """
    Returns a logger with the given name and log level.

    :param logger_name: The name of the logger.
    :param log_level: The log level.
    :param propagate: Whether to propagate logs to the root logger.
    :param handler: The handler to use.
    :return: A logger.
    """
    logger = logging.getLogger(logger_name)
    logger.setLevel(log_level)
    logger.addHandler(handler)
    logger.propagate = propagate
    return logger
